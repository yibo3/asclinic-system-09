#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sensor_msgs.msg import LaserScan

import rospy

# Respond to subscriber receiving a message
def laserScanSubscriberCallback(self, msg):
    rospy.loginfo("Message receieved with angle_min = " + str(msg.angle_min) + " [rad], angle_max = " + str(msg.angle_max) + " [rad], range_min = " + str(msg.range_min) + " [m], range_max = " + str(msg.range_max) + " [m]")

if __name__ == '__main__':
    # Initialise the node
    rospy.init_node("rp_lider")

    # Initialise a subscriber to the RPLidar scan
    rospy.Subscriber("/asc"+"/scan", LaserScan, laserScanSubscriberCallback)

    # Spin as a single-threaded node
    rospy.spin()

